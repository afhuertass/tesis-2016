
#include <arrayfire.h>
#include <LBD3Q15.h>
#include <ObstacleLoader.h>
#include <ForceModule.h>
#include <vector>
#include <arrayfire.h>

// Modelo 3d para lattice bolztman utlizando el metodo entrpico 
using namespace af;
LatticeBolztmannD3Q15::LatticeBolztmannD3Q15(int lx,int ly, int lz){
  //constructor
  //this->q = 15;
  this->q = 15;
  this->Lx=lx; // tamaño del lattice
  this->Ly=ly;
  this->Lz=lz;
  // la primera dimension es q = 15 el lattice
  
  fs = af::randu( q , Lx , Ly , Lz);
 
  jx = af::randu( Lx , Ly , Lz);
  jy = af::randu( Lx , Ly , Lz);
  jz = af::randu( Lx , Ly , Lz);
  rhos =  array( Lx, Ly, Lz);
  w = af::constant( 0 , 15 , f32);
  // velocidades discretas x
  int vx[] = {0,1,0,0,-1,0,0,1,-1,1,1,-1,1,-1,-1  }; 
  int vy[] = {0,0,1,0,0,-1,0,1,1,-1,1,-1,-1,1,-1  };
  int vz[] = {0,0,0,1,0,0,-1,1,1,1,-1,1,-1,-1,-1 };

  int ws[] = {16,8,8,8,8,8,8,1,1,1,1,1,1,1,1};
  this->ConditionsU = af::constant(1, Lx,Ly,Lz,f32);
  
  vel_x = array( q , vx);
  vel_y = array (q , vy);
  vel_z = array(q , vz);
  w = array( q , ws);

  w = 1.0/72*w;
  //vel_x =  af::tile( vel_x , 1 , Lx , Ly , Lz  );
  /*
  w(0) = 16; w(1)=w(2)=w(3)=w(4)=w(5)=w(6) = 8;
  w(7)=w(8)=w(9)=w(10)=w(11)=w(12)=w(13)=w(14)=1;
  w = 1/72.0*w;
  */
  V[0][0] = 0; V[0][1] = 1; V[0][2] = 0; V[0][3] = 0 ; V[0][4] = -1;
  V[0][5] = 0; V[0][6] = 0; V[0][7] = 1; V[0][8] = -1 ; V[0][9] = 1;
  V[0][10] = 1; V[0][11] = -1; V[0][12] = 1; V[0][13] = -1 ; V[0][14] = -1;
  
  V[1][0] = 0; V[1][1] = 0; V[1][2] = 1; V[1][3] = 0 ; V[1][4] = 0;
  V[1][5] = -1; V[1][6] = 0; V[1][7] = 1; V[1][8] = 1 ; V[1][9] = -1;
  V[1][10] = 1; V[1][11] = -1; V[1][12] = -1; V[1][13] = 1 ; V[1][14] = -1;

  V[2][0] = 0; V[2][1] = 0; V[2][2] = 0; V[2][3] = 1 ; V[2][4] = 0;
  V[2][5] = 0; V[2][6] = -1; V[2][7] = 1; V[2][8] = 1 ; V[2][9] = 1;
  V[2][10] = -1; V[2][11] = 1; V[2][12] = -1; V[2][13] = -1 ; V[2][14] = -1;

 
}
void LatticeBolztmannD3Q15::Inicie(float r0 , float Ux0 , float Uy0 , float Uz0){
  array rh0 = af::constant( r0 ,1 ,Lx, Ly, Lz , f32);
  array Uxs = af::constant( Ux0,1 ,Lx , Ly , Lz , f32);
  array Uys = af::constant( Uy0,1, Lx , Ly , Lz , f32);
  array Uzs = af::constant( Uz0,1, Lx , Ly , Lz , f32);

  //f = this->feq( rh0, Uxs , Uys, Uzs);
  //SetConditions( Uxs , Uys, Uzs );
  int i = 0;
  
  ///  AGREGAR EL INICIE BIEN BIEN  .... INICIALIZAR LAS FEQ
 
  /*float Re = 100;
  float ulb = 0.03;
  float r = 25;
  float nulb = ulb*r/Re;
  this->beta = 1/(3.0*nulb + 0.5); 

  */
 
  fs = feq2( rh0, Uxs , Uys, Uzs);
  
}
void LatticeBolztmannD3Q15::SetConditions(array &Ux, array &Uy , array &Uz){
  
// poiseullie 
 // plano x = 0 la velocidad es maxima
  
 //  Ventiladores en la entrada
 //   Ux(0,span,span ) = 0.03; // this->UMAX;
 //   Uy(0,span,span ) = 0;
 //   Uz(0,span,span ) = 0;
  
  // LID DRIVEN CAVITY  
  //plano y = 0

  Uy(span , 0 , span) = 0;

  // plano y = Ly , velocidad
  
  Uy(span , Ly-1 , span) = 0;

  // plano x = 0

  Ux(0,span,span ) = 0.0;
  Uy(0,span,span ) = 0;
  Uz(0,span,span ) = 0;
  

   // plano x = Lx
  Ux(Lx-1,span,span ) = 0.0;
  Uy(Lx-1,span,span ) = 0;
  Uz(Lx-1,span,span ) = 0;
  
  // plano z = 0
  Ux(span,span,0 ) = 0.0;
  Uy(span,span, 0) = 0;
  Uz(span,span,0 ) = 0;
 
   // plano z = Lz
  Ux(span,span,Lz-1 ) = 0.03;
  Uy(span,span, Lz-1) = 0;
  Uz(span,span, Lz-1) = 0;
  
}
void LatticeBolztmannD3Q15::setArrayC(array c){
  this->ConditionsU = c;
}
array LatticeBolztmannD3Q15::feq2( array &rhos, array &Uxs , array &Uys , array &Uzs){ 
  
  
  return (  af::tile( rhos ,  q )*af::tile( w , 1 , Lx , Ly , Lz ))*aux(vel_x, Uxs)*aux(vel_y,Uys)*aux(vel_z,Uzs);
  //return  (  af::tile( rhos ,  q )*af::tile( w , 1 , Lx , Ly , Lz ));
  // funcion de quilibrio entropico , tosi2007 
}
array LatticeBolztmannD3Q15::feq3(array &rhos , array &Uxs , array &Uys, array &Uzs  ){
  array UpCi = (af::tile(Uxs , q )*af::tile(vel_x, 1, Lx, Ly, Lz) + af::tile(Uys , q )*af::tile(vel_y,1, Lx, Ly, Lz) +  (af::tile(Uzs , q )*af::tile(vel_z,1, Lx, Ly, Lz))  );
  array U2 = af::tile(Uxs*Uxs + Uys*Uys + Uzs*Uzs , q);
  
  return ( af::tile( rhos ,q)*af::tile(w,1,Lx,Ly,Lz )*( 1 + 1.0/3*UpCi + 1.0/16*(af::pow(UpCi,2) - U2 )  ) );
  
}

array LatticeBolztmannD3Q15::rho(){
  // calcular rho , sumar fs en la direccion de la primera componente.
  return ( af::sum( fs , 0 ) );
  
  
}
array LatticeBolztmannD3Q15::Jx(){
  //array jx = af::sum(  fs*af::tile( vel_x , 1 , Lx , Ly , Lz  ), 0  );
  
  array jx = fs*af::tile( vel_x , 1 , Lx , Ly , Lz  );
  
  af::sum( jx , 0 );
  jx.eval();
  return ( jx );
  
}
array LatticeBolztmannD3Q15::Jy(){
 
  return ( af::sum(   fs*af::tile( vel_y , 1 , Lx , Ly , Lz  ), 0  )  ) ;
}
array LatticeBolztmannD3Q15::Jz(){
  return (  af::sum(   fs*af::tile( vel_z , 1 , Lx , Ly , Lz  ) , 0  )   );

}
array  LatticeBolztmannD3Q15::aux(array &vels, array &Ua){
  //af_print( af::tile( vels , 1 , Lx , Ly , Lz  ) );
  
  //af::pow( af::tile( (2*Ua+af::sqrt(1+3*Ua ))/(1-Ua) , q ) ,   af::tile( vels , 1 , Lx , Ly , Lz  )    );
  // af_print (af::tile( (2*Ua+af::sqrt(1+3*Ua ))/(1-Ua) , q )); 
  array f2v = af::tile( (2*Ua+af::sqrt(1+3*(Ua*Ua) ))/(1-Ua) , q );
  array f3v = af::tile( vels , 1 , Lx , Ly , Lz  );
  array res = af::pow( f2v ,  f3v   ); 
  return  res;

  // ( af::tile( 2-af::sqrt( 1 + af::pow(Ua,2 ) ) , q  ))*
}

void LatticeBolztmannD3Q15::Colission(){
  
  float rex = 1/0.53; // tau = 0.53
  //array rho = this->rho(); 
  array Ux =  this->Jx(); //rho ;
  //array Uy = this->Jy()/rho ;
  //array Uz =  this->Jz()/rho ;
  
  
  //af_print( feq2( rho, Ux , Uy , Uz));
  
  //fs = fs + beta*(feq2(rho,Ux,Uy,Uz ) - fs );
  
  //fs.eval();
}

void LatticeBolztmannD3Q15::Adveccion(){
 
  //_print( fs );
  array ff(q, Lx , Ly , Lz);
  //std::cout <<" WTF:?" << f << std::endl;
  /*
  for( int i = 0 ; i < q ; i++) { 
    //_print( af::shift( fs(1,span,span,span) , 0 , V[0][1] , V[1][1] , V[2][1] ) );
    
    // fs( i , span,span,span) = 
    af::join( 0 , af::shift( fs(i,span,span,span) , 0 , V[0][i] , V[1][i] , V[2][i] ) , ff)  ;
  }
  fs = ff;
  //_print( fs ); 
  */
  gfor ( seq i , q ){
    fs( i ,span,span,span) = af::shift( fs(i,span,span,span) , 0 , vel_x(i).scalar<int>() ,vel_y(i).scalar<int>()  ,vel_z(i).scalar<int>()  );
    //fs( i ,span,span,span) = af::shift( fs(i,span,span,span) , 0 , af::sum<int>( vel_x(i)) , af::sum<int>( vel_y(i)) , af::sum<int>( vel_z(i)) );
  }
}


void LatticeBolztmannD3Q15::SaveFs(std::string route_fs){
  
  std::ofstream file_fs(route_fs+"fs.txt");
  if( !file_fs.is_open() ) {
    std::cout << "ERROR ABRIENDO FS" << std::endl;
    return ;
  }
  file_fs << Lx << " " << Ly << " " << Lz << " " << q << "\n";
  float *fi;
  for( int alpha = 0 ; alpha < q ; alpha++){
    fi = this->fs.host<float>();
    for(int x = 0 ; x < Lx ; x ++)
      for(int y = 0 ; y< Ly  ;y++ ) 
	for(int z = 0 ; z < Lz ; z++){
	  float fval = fi[x + Ly*(y + Lx*z) ];
	  file_fs << x << " " << y << " " << z << " " << alpha << " " << fval <<"\n"; 
	  
	}
  }
  file_fs.close();
  // Guardando velocidades
  std::ofstream file_vs(route_fs+"vs.txt");
  if( !file_vs.is_open()){
    std::cout << "ERROR ABRIENDO VS" << std::endl;
    return;
  }
  
  for( int alpha = 0 ; alpha < q ; alpha++){
    file_vs << V[0][alpha] << " " << V[1][alpha]<< " " << V[2][alpha] << "\n";
  }
  file_vs.close();
  
  delete[] fi;
  
}

void LatticeBolztmannD3Q15::saveVTK(std::string route){
  // guardar un legacy file para
  array rho = this->rho();
  array Ux = (this->Jx()/rho)*this->ConditionsU; 
  array Uy = (this->Jy()/rho)*this->ConditionsU;
  array Uz = (this->Jz()/rho)*this->ConditionsU;
  std::ofstream file_vtk( route );
  if( !file_vtk.is_open()){
    std::cout << "# error abriendo archivo " << route << std::endl; 
    return ;
  }
  file_vtk << "# vtk DataFile Version 3.0 \n";
  file_vtk << "vtk output \n ";
  file_vtk << "ASCII \n";
  file_vtk << "DATASET STRUCTURED_GRID \n";
  file_vtk << "DIMENSIONS " << Lx << " " << Ly << " " <<  Lz <<"\n";
  file_vtk << "POINTS " << Lx*Ly*Lz << " float \n";
  for(int k = 0; k < Lz ; k++)
    for( int j = 0 ; j < Ly ; j++)
      for( int i = 0 ; i < Lx ; i++) 
	file_vtk << i << " " << j << " " << k << "\n";

  // hecha la grilla... a por los valores de las velocidades
  file_vtk << "POINT_DATA " << Lx*Ly*Lz << "\n";

  /// Ux field 
  float *h_ux = Ux.host<float>();
  file_vtk << "SCALARS Ux float \n";
  file_vtk << "LOOKUP_TABLE default \n";
  int index = 0;
  for( int k = 0 ; k < Lz ; k++)
    for( int j = 0 ; j < Ly ; j++)
      for(int i = 0; i < Lx ; i++) { 
	file_vtk << h_ux[ index  ] << " \n";
	index++;
      }
 
  
  float *h_uy = Uy.host<float>();
  index = 0;
  file_vtk << "SCALARS Uy float \n";
  file_vtk << "LOOKUP_TABLE default \n";
  for( int k = 0 ; k < Lz ; k++)
    for( int j = 0 ; j < Ly ; j++) 
      for( int i = 0 ; i < Lx ; i++){ 
	file_vtk << h_uy[ index ] << "\n";
	index++;
      }
  
  float *h_uz = Uz.host<float>();
  index = 0;
  file_vtk << "SCALARS Uz float \n";
  file_vtk << "LOOKUP_TABLE default \n";
  for( int k = 0 ; k < Lz ; k++)
    for( int i = 0 ; i < Lx ; i++)
      for( int j = 0 ; j < Ly ; j++)
	{
	  file_vtk << h_uz[ index ] << "\n";
	  index++;
      }
  

  file_vtk << "VECTORS VelocityField float \n";
  index = 0;
  for( int k = 0 ; k < Lz ; k++)
    for( int j = 0 ; j < Ly ; j++)
      for( int i = 0 ; i < Lx ; i++)
      {
	file_vtk << h_ux[ index ] << " " << h_uy[index ] << " " << h_uz[ index ] << " \n";
	index++;
      }
  
  file_vtk.close();
  delete [] h_ux;
  delete [] h_uy;
  delete [] h_uz;
}
void LatticeBolztmannD3Q15::Eval(){

  fs.eval();
}
void LatticeBolztmannD3Q15::Run(int steps){
  float tau = 0.53;
  
  for( int i = 0 ; i < steps; i++){
    //std::cout << "Step:" << i << std::endl;
    af::timer timeit = af::timer::start();
    //*af::tile( vel_x , 1 , Lx , Ly , Lz )
    
    jx =   af::sum( fs* af::tile( vel_x , 1 , Lx , Ly , Lz ) , 0) ; 
    jy =  af::sum( fs*af::tile( vel_y , 1 , Lx , Ly , Lz ) ,0 );
    jz =  af::sum( fs*af::tile( vel_z , 1 , Lx , Ly , Lz ) ,0);
    rhos = af::sum( fs , 0 );
    //array vs = af::join( 2 , jx , jy , jz ) ;
    
     array vs = af::join( 0 , jx , jy , jz )/af::tile( rhos , 3) ;
    
    //af_print ( vs ); 
    array ux = vs(0,span,span,span); // x component of the velocity
    array uy = vs(1,span,span); // y component of the velocity
    array uz = vs(2,span,span); // z component of the velocity
    /*
    array feq =  feq2( rhos ,  ux , uy , uz ); // feq
    fs += (1/tau)*(feq - fs);
     
    Adveccion();
    */
    // Adveccion();
    double t = af::timer::stop( timeit );
    std::cout << i << " " << t << std::endl;
  }
  return ;
}
