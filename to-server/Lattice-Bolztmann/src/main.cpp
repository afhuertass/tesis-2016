#include <LBD3Q15.h>
#include <ForceModule.h>
#include <ObstacleLoader.h>
void ensayos2(){
  
  int Lx, Ly , Lz ;
  Lx = Ly = Lz = 68;
  array f = af::randu( 15 , Lx , Ly , Lz );
  
  for( int i = 0 ; i < 1000 ; i++){
    array vs = af::sum( f , 0 );
    
  }
  std::cout << "Ensayos 2 " << std::endl;
}
void ensayos1(){
  
  int pasos = 1000;
  int Lx, Ly, Lz;
  Lx = Ly = 100;
  Lz = 100;
  LatticeBolztmannD3Q15 fluid ( Lx, Ly, Lz );
  af::timer timeit = af::timer::start();
  fluid.Inicie( 1 , 0.03 , 0, 0);

  fluid.Run( pasos);
  
  double t = af::timer::stop( timeit );
  std::cout << "#Tiempo t:" << t << std::endl;
  double MUPS = (Lx*Ly*Lz)/1e6;
  MUPS = (MUPS*pasos)/t;
 
  std::cout << "#MUPS " << MUPS << std::endl;
} 

void nonEntropicCall() {
 int pasos = 100;
  
   //af::info();
  int Lx, Ly, Lz ;
  
  Lx = 10; 
  Ly = Lz = 10;
  LatticeBolztmannD3Q15 fluid(Lx,Ly, Lz);
  fluid.Inicie( 1 , 0 , 0 , 0 );
  
  af::timer timeit = af::timer::start();
  
  for( int p = 0 ; p < pasos ; p++){
    
    //if ( p % 100 == 0 ) std::cout << "#Step:" << p << std::endl;
    fluid.Adveccion();
    fluid.Colission();
  }
  
  double t = af::timer::stop( timeit );
  double MUPS = (Lx*Ly*Lz)/1e6;
  MUPS = (MUPS*pasos)/t;
  
  std::cout << "#MUPS " << MUPS << std::endl;
  
  //fluid.saveVTK("./simulation-runs/Fs/steps=500/esfera-Re=50.vtk");
  //fluid.SaveFs(rutaFs);

  
  //fluid.saveVTK("./lid-driven-re=200.vtk");
  
  // ForceModule  fm(rutaFs);
  // fm.calculateForce();
 
}
void EntropicCall(){
  /*
  int pasos = 1; 
  int Lx, Ly, Lz;
  Lx = Ly = Lz = 5;
  ELatticeBolztmannD3Q15 Efluid( Lx , Ly , Lz );
  Efluid.Inicie( 1 , 0.03 , 0 , 0 );
  af::timer timeit = timer::start();
  for ( int i = 0 ; i < pasos ; i++){
    
    Efluid.Adveccion();
    Efluid.Colission();
  }
  double t = af::timer::stop(timeit);
  double MUPS = (Lx*Ly*Lz)/1e6;
  MUPS = (MUPS*pasos)/t;
  std::cout << "#MUPs:" << MUPS << std::endl;
  */
  //Efluid.saveVTK("./entropic-lid-driven.vtk");
} 


int main(int argc , char * argv[] ){
  
  int device = argc > 1 ? atoi(argv[1]) : 0 ;
  af::setDevice(device);

  //nonEntropicCall();
  ensayos1();

  std::cout << "#Salio de funcion ensayos.... "<< std::endl;
}
