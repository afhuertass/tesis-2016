#include <arrayfire.h>
// armazon de la clase encargada de las simulaciones
using namespace af;
class LatticeBolztmannD3Q15{
 private:
  int q;
  double beta;
  int Lx,Ly,Lz; // variables del lattice
  float V[3][15]; // velocidades del lattice
  array f; // funciones de equilibrio
  array Vel_x, Vel_y , Vel_z ;
  array w; // funciones de peso
 public:
  LatticeBolztmannD3Q15(int Lx, int Ly, int Lz); // constructor - reciba el tamaño del lattice
  void Inicie(float r0, float Ux0, float Uy0, float Uz0); // reciba la densidad y velocidades cero
  
  array feq( array &rhos, array &Uxs , array &Uys , array &Uzs);
  
  array chi( array &rho , array &Ux, array &Uy , array &Uz);
  array zita_a( array &Ua );
  array rho();
  array Jx();
  array Jy();
  array Jz();
  
  void Colission(void);
  void Adveccion(void);
  
  void Print(void);

};
