#include <arrayfire.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

class ObstacleLoader{
  

public:
  ObstacleLoader();
 af::array loadFile(std::string file, int grid_s  );
};

