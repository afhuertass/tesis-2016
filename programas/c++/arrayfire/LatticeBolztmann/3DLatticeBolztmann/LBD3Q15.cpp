 #include <arrayfire.h>
#include "latticed3q15.h"


// Modelo 3d para lattice bolztman utlizando el metodo entrpico 
using namespace af;
LatticeBolztmannD3Q15::LatticeBolztmannD3Q15(int lx,int ly, int lz){
  //constructor
  this->q = 15;
  this->Lx=lx; // tamaño del lattice
  this->Ly=ly;
  this->Lz=lz;
  
  this->f = af::constant(0 , Lx , Ly, Lz, q , f32);
  // arreglo de funciones de equilibrio
  Vel_x = af::constant(0 , Lx , Ly, Lz, q , f32);
  Vel_y = af::constant(0 , Lx , Ly, Lz, q , f32);
  Vel_z = af::constant(0 , Lx , Ly, Lz, q , f32);
  w = af::constant(0 , q , f32);
  // Poblar las Vel_i con  las velocidades del Lattice

  Vel_x.slice(0) = af::constant( 0, Vel_x.slice(0).dims() );
  Vel_x.slice(1) = af::constant( 1, Vel_x.slice(1).dims() );
  Vel_x.slice(2) = af::constant( 0, Vel_x.slice(2).dims() );
  Vel_x.slice(3) = af::constant( 0, Vel_x.slice(3).dims() );
  Vel_x.slice(4) = af::constant( -1, Vel_x.slice(4).dims() );
  Vel_x.slice(5) = af::constant( 0, Vel_x.slice(5).dims() );
  Vel_x.slice(6) = af::constant( 0, Vel_x.slice(6).dims() );
  Vel_x.slice(7) = af::constant( 1, Vel_x.slice(7).dims() );
  Vel_x.slice(8) = af::constant( -1, Vel_x.slice(8).dims() );
  Vel_x.slice(9) = af::constant( 1, Vel_x.slice(9).dims() );
  Vel_x.slice(10) = af::constant( 1, Vel_x.slice(10).dims() );
  Vel_x.slice(11) = af::constant( -1, Vel_x.slice(11).dims() );
  Vel_x.slice(12) = af::constant( 1, Vel_x.slice(12).dims() );
  Vel_x.slice(13) = af::constant( -1, Vel_x.slice(13).dims() );
  Vel_x.slice(14) = af::constant( -1, Vel_x.slice(14).dims() );

  /// Velocidades del lattice en y
  Vel_y.slice(0) = af::constant( 0, Vel_y.slice(0).dims() );
  Vel_y.slice(1) = af::constant( 0, Vel_y.slice(1).dims() );
  Vel_y.slice(2) = af::constant( 1, Vel_y.slice(2).dims() );
  Vel_y.slice(3) = af::constant( 0, Vel_y.slice(3).dims() );
  Vel_y.slice(4) = af::constant( 0, Vel_y.slice(4).dims() );
  Vel_y.slice(5) = af::constant( -1, Vel_y.slice(5).dims() );
  Vel_y.slice(6) = af::constant( 0, Vel_y.slice(6).dims() );
  Vel_y.slice(7) = af::constant( 1, Vel_y.slice(7).dims() );
  Vel_y.slice(8) = af::constant( 1, Vel_y.slice(8).dims() );
  Vel_y.slice(9) = af::constant( -1, Vel_y.slice(9).dims() );
  Vel_y.slice(10) = af::constant( 1, Vel_y.slice(10).dims() );
  Vel_y.slice(11) = af::constant( -1, Vel_y.slice(11).dims() );
  Vel_y.slice(12) = af::constant( -1, Vel_y.slice(12).dims() );
  Vel_y.slice(13) = af::constant( 1, Vel_y.slice(13).dims() );
  Vel_y.slice(14) = af::constant( -1, Vel_y.slice(14).dims() );

  /// velocidades del Lattice en z 
  Vel_z.slice(0) = af::constant( 0, Vel_z.slice(0).dims() );
  Vel_z.slice(1) = af::constant( 0, Vel_z.slice(1).dims() );
  Vel_z.slice(2) = af::constant( 0, Vel_z.slice(2).dims() );
  Vel_z.slice(3) = af::constant( 1, Vel_z.slice(3).dims() );
  Vel_z.slice(4) = af::constant( 0, Vel_z.slice(4).dims() );
  Vel_z.slice(5) = af::constant( 0, Vel_z.slice(5).dims() );
  Vel_z.slice(6) = af::constant( -1, Vel_z.slice(6).dims() );
  Vel_z.slice(7) = af::constant( 1, Vel_z.slice(7).dims() );
  Vel_z.slice(8) = af::constant( 1, Vel_z.slice(8).dims() );
  Vel_z.slice(9) = af::constant( 1, Vel_z.slice(9).dims() );
  Vel_z.slice(10) = af::constant( -1, Vel_z.slice(10).dims() );
  Vel_z.slice(11) = af::constant( 1, Vel_z.slice(11).dims() );
  Vel_z.slice(12) = af::constant( -1, Vel_z.slice(12).dims() );
  Vel_z.slice(13) = af::constant( -1, Vel_z.slice(13).dims() );
  Vel_z.slice(14) = af::constant( -1, Vel_z.slice(14).dims() );
  
  w(0) = 16; w(1)=w(2)=w(3)=w(4)=w(5)=w(6) = 8;
  w(7)=w(8)=w(9)=w(10)=w(11)=w(12)=w(13)=w(14)=1;
  w = 1/72.0*w;
  
  V[0][0] = 0; V[0][1] = 1; V[0][2] = 0; V[0][3] = 0 ; V[0][4] = -1;
  V[0][5] = 0; V[0][6] = 0; V[0][7] = 1; V[0][8] = -1 ; V[0][9] = 1;
  V[0][10] = 1; V[0][11] = -1; V[0][12] = 1; V[0][13] = -1 ; V[0][14] = -1;
  
  V[1][0] = 0; V[1][1] = 0; V[1][2] = 1; V[1][3] = 0 ; V[1][4] = 0;
  V[1][5] = -1; V[1][6] = 0; V[1][7] = 1; V[1][8] = 1 ; V[1][9] = -1;
  V[1][10] = 1; V[1][11] = -1; V[1][12] = -1; V[1][13] = 1 ; V[1][14] = -1;

  V[2][0] = 0; V[2][1] = 0; V[2][2] = 0; V[2][3] = 1 ; V[2][4] = 0;
  V[2][5] = 0; V[2][6] = -1; V[2][7] = 1; V[2][8] = 1 ; V[2][9] = 1;
  V[2][10] = -1; V[2][11] = 1; V[2][12] = -1; V[2][13] = -1 ; V[2][14] = -1;

  float Re = 100 ; // numero de Reynolds
  float viscosidad = 1/Re;
  float tau = 3*(viscosidad);
  
  float dx = 1/Lx;
  float dt = dx*dx;

  this->beta = dt/(2*tau +dt);

}
void LatticeBolztmannD3Q15::Inicie(float r0 , float Ux0 , float Uy0 , float Uz0){
  array rh0 = af::constant( r0 , Lx, Ly, Lz , f32);
  array Uxs = af::constant( Ux0 , Lx , Ly , Lz , f32);
  array Uys = af::constant( Ux0 , Lx , Ly , Lz , f32);
  array Uzs = af::constant( Ux0 , Lx , Ly , Lz , f32);

  f = this->feq( rh0, Uxs , Uys, Uzs);
}
array LatticeBolztmannD3Q15::feq( array &rhos, array &Uxs , array &Uys , array &Uzs){
  array feq(Lx,Ly,Lz, q  , f32);
  array chi = this->chi( rhos, Uxs, Uys, Uzs);
  gfor(seq i , q) {
    //w(i)*chi*zita_a(Uxs)*zita_a(Uys)*zita_a(Uzs);
    feq(span,span,span,i) = w(i)*chi*zita_a(Uxs)*zita_a(Uys)*zita_a(Uzs);
  }
  return feq;
}
array LatticeBolztmannD3Q15::rho(){
  return (af::sum(f,3)); // suma a lo largo de la tercera componente
  
}
array LatticeBolztmannD3Q15::Jx(){
  return (af::sum(f*Vel_x , 3 ) );
}
array LatticeBolztmannD3Q15::Jy(){
  return (af::sum(f*Vel_y , 3));
  
}
array LatticeBolztmannD3Q15::Jz(){
  return (af::sum(f*Vel_z , 3));
}
array LatticeBolztmannD3Q15::chi( array &rho, array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  return (rho*(1-3.0/2*U2+9.0/8*af::pow(U2,4 )) );
  
}
array LatticeBolztmannD3Q15::zita_a(array &Ua){
    array unity = af::constant(1 , Lx,Ly,Lz , f32);
    return ( unity - 3*Ua +9.0/2*af::pow(Ua,2) + 9.0/2*af::pow(Ua,3)+27.0/8*af::pow(Ua,4));
}

void LatticeBolztmannD3Q15::Colission(){
  float alpha=2;
  array rho = this->rho();
  array Ux = this->Jx()/rho ;
  array Uy = this->Jy()/rho;
  array Uz = this->Jz()/rho;
  f +=  alpha*beta*( feq(rho,Ux,Uy,Uz ) - f  );
}

void LatticeBolztmannD3Q15::Adveccion(){
  std::vector<array> fs(q); 
  for(int i = 0; i< q ; i++){
    fs.at(i)   =  shift( f(span,span,span,i) , V[1][i] , V[0][i] , V[2][i] ) ;
   
  }

}
void LatticeBolztmannD3Q15::Print(void){
 
  
}
int main(int argc , char *argv[] ){
  int pasos = 1000;
  int device = argc > 1 ? atoi(argv[1]) : 0 ;
  af::setDevice(device);
  af::info();
  int Lx, Ly , Lz;
  Lx = Ly = Lz = 20;
  double memory_size = 4*(Lx*Ly*Lz*15);
  std::cout << "#Memoria requerida:" << memory_size << std::endl;
  std::cout << "#Version  1 ( arrys 4D ) " << std::endl;
  LatticeBolztmannD3Q15 fluid(Lx,Lx,Lx);
  fluid.Inicie( 1 , 0.5 , 0.5 ,0.5);
  timer reloj = timer::start();
  for ( int i = 0 ; i< pasos ; i++){
    fluid.Colission();
    fluid.Adveccion();
  }
  double t = af::timer::stop(reloj);
  std::cout << "LUPS  " << (pasos*Lx*Ly*Lz)/t << std::endl;
  return 0;
}
