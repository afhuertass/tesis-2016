// esta clase pretende cargar los obstaculos generados 
/*#include <arrayfire.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
*/
#include "ObstacleLoader.h"
#include <iostream>
ObstacleLoader::ObstacleLoader(){
  
}
af::array ObstacleLoader::loadFile(std::string file , int grid_s){
  
  // vamos a leer linea por linea, primero para contar las lineas

  std::ifstream archivo(file);
  
  std::string line;
  int size = 0, index = 0;
  float * obj;
  if(archivo.is_open()){
    while( getline(archivo,line ) ){
      if(line == "") continue;
      size +=1;
    }
    // crear el arreglo de ese tamaño
    std::cout << "#tamaño: " << size << std::endl;
    archivo.close();
    std::ifstream archivo2(file);
    
    obj = new float[size];
    while( std::getline( archivo2 ,line)){
      std::stringstream linestream(line);
      std::string data;
      float v1,v2,v3,v4;
      linestream >> v1 >> v2 >> v3 >> v4;
      //std::cout << v4 << std::endl;
      obj[index] = v4;
      index++;
    }
   
    
    af::array af_obj(grid_s, grid_s , grid_s, obj);
    
    return af_obj;
    
  }else { //raise exception or just say something went wrong
    
    std::cout<< "# Imposible cargar archivo" << std::endl;
    return af::constant(0, 10); // just whatever
    
  } 
  //return 0.0;
}


