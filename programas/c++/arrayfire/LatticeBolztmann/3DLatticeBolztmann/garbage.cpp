/*
 este archivo esta destinado a contener funciones que por alguna razon deje de usar pero me da miedo borrar porque pueda llegar a necesitar. 



 */

/// funcions chi del paper de D3Q15 entropico 

array LatticeBolztmannD3Q15::chi( array &rho, array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  array II = af::constant(1 , Lx,Ly,Lz , f32);

  return (rho*(II-3.0/2*U2+9.0/8*af::pow(U2,2 ) + chi6(Ux,Uy,Uz ) + chi8(Ux ,Uy ,Uz) ) );
  
}
array LatticeBolztmannD3Q15::chi6( array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  array Ux2 = af::pow(Ux,2); array Uy2 = af::pow(Uy,2); array Uz2 = af::pow(Uz,2);
  return ( 27.0/16*(-af::pow(U2,3)+2*(Uy2 +Uz2 )*(U2*Ux2 + Uy2*Uz2 )+20*Ux2*Uy2*Uz2 ) );
}
array LatticeBolztmannD3Q15::chi8( array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  array Ux2 = af::pow(Ux,2); array Uy2 = af::pow(Uy,2); array Uz2 = af::pow(Uz,2);
  array Ux4 = af::pow(Ux,4); array Uy4 = af::pow(Uy,4); array Uz4 = af::pow(Uz,4);
  array Ux8 = af::pow(Ux2,4); array Uy8 = af::pow(Uy2,4) ; array Uz8 = af::pow(Uz2,4);
  return ( 81.0/128*af::pow(U2,4) + 81.0/32*(Ux8+Uy8+Uz8-36*Ux2*Uy2*Uz2*U2-Ux4*Uy4-Uy4*Uz4-Ux4*Uz4 ) );
}

array LatticeBolztmannD3Q15::zita_a(array &Ua){
    array unity = af::constant(1 , Lx,Ly,Lz , f32);
    return ( unity + 3*Ua +9.0/2*af::pow(Ua,2) + 9.0/2*af::pow(Ua,3)+27.0/8*af::pow(Ua,4));
}

array LatticeBolztmannD3Q15::feq(int i ,  array &rhos, array &Uxs , array &Uys , array &Uzs){
  // array feq(Lx,Ly,Lz, f32);
  array chi = this->chi( rhos, Uxs, Uys, Uzs);
    //w(i)*chi*zita_a(Uxs)*zita_a(Uys)*zita_a(Uzs);
  
  // return ( w(0)*chi*zita_a(Uxs)*zita_a(Uys)*zita_a(Uzs)    );
  return (W[i]*chi*af::pow(zita_a(Uxs), V[0][i])*af::pow(zita_a(Uys), V[1][i])*af::pow(zita_a(Uzs), V[2][i]) )  ;
}
