var group__linalg__mat =
[
    [ "BLAS operations", "group__blas__mat.htm", "group__blas__mat" ],
    [ "Linear solve and least squares", "group__lapack__solve__mat.htm", "group__lapack__solve__mat" ],
    [ "Matrix factorizations and decompositions", "group__lapack__factor__mat.htm", "group__lapack__factor__mat" ],
    [ "Matrix operations", "group__lapack__ops__mat.htm", "group__lapack__ops__mat" ]
];