var group__construct__mat =
[
    [ "af_create_array", "group__construct__mat.htm#ga834be32357616d8ab735087c6f681858", null ],
    [ "af_create_handle", "group__construct__mat.htm#ga3b8f5cf6fce69aa1574544bc2d44d7d0", null ],
    [ "af_device_array", "group__construct__mat.htm#ga751d919b6bc62e4f78dba3b088d75d18", null ],
    [ "array", "group__construct__mat.htm#ga9cbcfcbf0173e1edaf4094bb36b34b31", null ],
    [ "array", "group__construct__mat.htm#gad34fcf3e619cabf1cd107f72c571f091", null ],
    [ "array", "group__construct__mat.htm#gacfba46b754d8e3e5371d9bc78e7ba766", null ],
    [ "array", "group__construct__mat.htm#ga73cb9e2360cecbc511b87abf76b6d631", null ],
    [ "array", "group__construct__mat.htm#ga1f0b51e20111680c7fe3c74d54c982dd", null ],
    [ "array", "group__construct__mat.htm#ga69c9fa684e0b0beaf657ac1dc03afa56", null ],
    [ "array", "group__construct__mat.htm#ga6bc7bac9bb52349c198fa0861b5004b7", null ],
    [ "array", "group__construct__mat.htm#ga5686344bf8a49be5286892998d309619", null ],
    [ "array", "group__construct__mat.htm#ga8c330c41d6e06b0dea9377ef02762c6f", null ],
    [ "array", "group__construct__mat.htm#gaa2ebe6a7b991fbe6231321138e79121c", null ],
    [ "array", "group__construct__mat.htm#gabd3d95b130bdb2d7e713414687e6b15a", null ],
    [ "array", "group__construct__mat.htm#gaaa8fab98447367bc4eaf3d7bc61d8ff5", null ],
    [ "array", "group__construct__mat.htm#ga1144078b1596e7d29f57b1a0a1c9b1a8", null ],
    [ "array", "group__construct__mat.htm#gab65f7a320bb53cb1f25de25c685ec0a0", null ],
    [ "array", "group__construct__mat.htm#ga4866e95e473b71b99ad842608744ac4f", null ]
];