var group__gfx__func__window =
[
    [ "af_destroy_window", "group__gfx__func__window.htm#ga4a8dff5e0ca0ac4505a999281fbbad44", null ],
    [ "af_grid", "group__gfx__func__window.htm#ga37fc7eb00ae11c25e1a60d341663d68d", null ],
    [ "af_is_window_closed", "group__gfx__func__window.htm#gac2068f5373f6ddc316a26382fb8d5b9c", null ],
    [ "af_set_position", "group__gfx__func__window.htm#ga87e2e2291f1642f4a2ef38a094d61d75", null ],
    [ "af_set_size", "group__gfx__func__window.htm#gabc9ce32cb2c0a7a71b48812db8ef822e", null ],
    [ "af_set_title", "group__gfx__func__window.htm#ga40859c4145276f77c83d37e2d3608cf4", null ],
    [ "af_show", "group__gfx__func__window.htm#ga50dae861324dca1cce9f583256f5a654", null ]
];