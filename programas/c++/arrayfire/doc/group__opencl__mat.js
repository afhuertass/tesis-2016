var group__opencl__mat =
[
    [ "afcl_get_context", "group__opencl__mat.htm#gad42de383f405b3e38d6eb669c0cbe2e3", null ],
    [ "afcl_get_device_id", "group__opencl__mat.htm#gaf7258055284e65a8647a49c3f3b9feee", null ],
    [ "afcl_get_queue", "group__opencl__mat.htm#gab1701ef4f2b68429eb31c1e21c88d0bc", null ],
    [ "array", "group__opencl__mat.htm#ga5434aaf76be37fae92ac5086315516f0", null ],
    [ "array", "group__opencl__mat.htm#ga032919598890f2ccc6b1835c213416c7", null ],
    [ "array", "group__opencl__mat.htm#ga752a709190f80c8342e2da6fc405af27", null ],
    [ "array", "group__opencl__mat.htm#gafad240e885df716a656bd6634db22626", null ],
    [ "array", "group__opencl__mat.htm#ga89507e0349f5d7ee068325b9b24c891b", null ],
    [ "getContext", "group__opencl__mat.htm#ga1984398db67a52977435e653bb842da7", null ],
    [ "getDeviceId", "group__opencl__mat.htm#gae40668cc7d19273f449a1628ac438182", null ],
    [ "getQueue", "group__opencl__mat.htm#ga4bdd87f7ee76ba2ac18a7e1719508d5d", null ]
];