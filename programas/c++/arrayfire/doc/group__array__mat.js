var group__array__mat =
[
    [ "Constructors of array class", "group__construct__mat.htm", "group__construct__mat" ],
    [ "Functions to create arrays.", "group__data__mat.htm", "group__data__mat" ],
    [ "Helper functions for arrays", "group__helper__mat.htm", "group__helper__mat" ],
    [ "Indexing operation on arrays", "group__index__mat.htm", "group__index__mat" ],
    [ "Managing devices in ArrayFire", "group__device__mat.htm", "group__device__mat" ],
    [ "Methods of array class", "group__method__mat.htm", "group__method__mat" ],
    [ "Move and Reorder array content", "group__manip__mat.htm", "group__manip__mat" ]
];