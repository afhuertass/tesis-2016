var group__device__func__mem =
[
    [ "af_device_gc", "group__device__func__mem.htm#ga182a33d34b3288c5cf5b88cd02468c56", null ],
    [ "af_device_mem_info", "group__device__func__mem.htm#gae633760aed4638f8a5ea333e0774ac84", null ],
    [ "af_get_device_ptr", "group__device__func__mem.htm#ga58fda2d491cd27f31108e699b5aef506", null ],
    [ "af_get_mem_step_size", "group__device__func__mem.htm#ga4c04df1ae248a6a8aa0a28263323872a", null ],
    [ "af_lock_device_ptr", "group__device__func__mem.htm#gac2ad5089cbca1a6cca8d87d42279c6a8", null ],
    [ "af_set_mem_step_size", "group__device__func__mem.htm#ga3be9c5ea9ee828868f5d906333a11499", null ],
    [ "af_unlock_device_ptr", "group__device__func__mem.htm#ga39817b0ba24db34f00c20cc3a20df6d4", null ],
    [ "deviceGC", "group__device__func__mem.htm#ga2fa2d8f09a01b92e840c8149630246d6", null ],
    [ "deviceMemInfo", "group__device__func__mem.htm#gae763d44f5c5cd80d4cf31fd044ee4e3d", null ],
    [ "getMemStepSize", "group__device__func__mem.htm#ga779bdc65de61e76e066d2cda263a814e", null ],
    [ "setMemStepSize", "group__device__func__mem.htm#ga9c79ca1f0e4dcf4168596d9602d7795c", null ]
];