var modules =
[
    [ "ArrayFire Functions by Category", "group__func__categories.htm", "group__func__categories" ],
    [ "Complete List of ArrayFire Functions", "group__arrayfire__func.htm", "group__arrayfire__func" ],
    [ "Graphics", "group__graphics__func.htm", "group__graphics__func" ],
    [ "operator()", "group__array__mem__operator__paren__one.htm", null ],
    [ "operator()", "group__array__mem__operator__paren__many.htm", null ]
];