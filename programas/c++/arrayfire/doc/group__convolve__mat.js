var group__convolve__mat =
[
    [ "convolve", "group__signal__func__convolve.htm", "group__signal__func__convolve" ],
    [ "convolve1", "group__signal__func__convolve1.htm", "group__signal__func__convolve1" ],
    [ "convolve2", "group__signal__func__convolve2.htm", "group__signal__func__convolve2" ],
    [ "convolve3", "group__signal__func__convolve3.htm", "group__signal__func__convolve3" ],
    [ "fftConvolve", "group__signal__func__fft__convolve.htm", "group__signal__func__fft__convolve" ],
    [ "fftConvolve2", "group__signal__func__fft__convolve2.htm", "group__signal__func__fft__convolve2" ],
    [ "fftConvolve3", "group__signal__func__fft__convolve3.htm", "group__signal__func__fft__convolve3" ]
];