var group__lapack__ops__mat =
[
    [ "det", "group__lapack__ops__func__det.htm", "group__lapack__ops__func__det" ],
    [ "inverse", "group__lapack__ops__func__inv.htm", "group__lapack__ops__func__inv" ],
    [ "norm", "group__lapack__ops__func__norm.htm", "group__lapack__ops__func__norm" ],
    [ "rank", "group__lapack__ops__func__rank.htm", "group__lapack__ops__func__rank" ]
];