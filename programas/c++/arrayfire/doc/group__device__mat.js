var group__device__mat =
[
    [ "alloc", "group__device__func__alloc.htm", "group__device__func__alloc" ],
    [ "array::device<T>", "group__device__func__device.htm", "group__device__func__device" ],
    [ "deviceInfo", "group__device__func__prop.htm", "group__device__func__prop" ],
    [ "deviceMemInfo", "group__device__func__mem.htm", "group__device__func__mem" ],
    [ "free", "group__device__func__free.htm", "group__device__func__free" ],
    [ "getDevice", "group__device__func__get.htm", "group__device__func__get" ],
    [ "getDeviceCount", "group__device__func__count.htm", "group__device__func__count" ],
    [ "info", "group__device__func__info.htm", "group__device__func__info" ],
    [ "isDoubleAvailable", "group__device__func__dbl.htm", "group__device__func__dbl" ],
    [ "pinned", "group__device__func__pinned.htm", "group__device__func__pinned" ],
    [ "setDevice", "group__device__func__set.htm", "group__device__func__set" ],
    [ "sync", "group__device__func__sync.htm", "group__device__func__sync" ]
];