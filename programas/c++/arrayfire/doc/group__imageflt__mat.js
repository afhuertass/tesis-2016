var group__imageflt__mat =
[
    [ "SAT", "group__image__func__sat.htm", "group__image__func__sat" ],
    [ "bilateral", "group__image__func__bilateral.htm", "group__image__func__bilateral" ],
    [ "maxfilt", "group__image__func__maxfilt.htm", "group__image__func__maxfilt" ],
    [ "meanshift", "group__image__func__mean__shift.htm", "group__image__func__mean__shift" ],
    [ "medfilt", "group__image__func__medfilt.htm", "group__image__func__medfilt" ],
    [ "minfilt", "group__image__func__minfilt.htm", "group__image__func__minfilt" ],
    [ "sobel", "group__image__func__sobel.htm", "group__image__func__sobel" ]
];