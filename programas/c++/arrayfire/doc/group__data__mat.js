var group__data__mat =
[
    [ "constant", "group__data__func__constant.htm", "group__data__func__constant" ],
    [ "diag", "group__data__func__diag.htm", "group__data__func__diag" ],
    [ "getSeed", "group__data__func__getseed.htm", "group__data__func__getseed" ],
    [ "identity", "group__data__func__identity.htm", "group__data__func__identity" ],
    [ "iota", "group__data__func__iota.htm", "group__data__func__iota" ],
    [ "lower", "group__data__func__lower.htm", "group__data__func__lower" ],
    [ "randn", "group__data__func__randn.htm", "group__data__func__randn" ],
    [ "randu", "group__data__func__randu.htm", "group__data__func__randu" ],
    [ "range", "group__data__func__range.htm", "group__data__func__range" ],
    [ "setSeed", "group__data__func__setseed.htm", "group__data__func__setseed" ],
    [ "upper", "group__data__func__upper.htm", "group__data__func__upper" ]
];