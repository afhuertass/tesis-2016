var group__reduce__mat =
[
    [ "alltrue", "group__reduce__func__all__true.htm", "group__reduce__func__all__true" ],
    [ "anytrue", "group__reduce__func__any__true.htm", "group__reduce__func__any__true" ],
    [ "count", "group__reduce__func__count.htm", "group__reduce__func__count" ],
    [ "max", "group__reduce__func__max.htm", "group__reduce__func__max" ],
    [ "min", "group__reduce__func__min.htm", "group__reduce__func__min" ],
    [ "product", "group__reduce__func__product.htm", "group__reduce__func__product" ],
    [ "sum", "group__reduce__func__sum.htm", "group__reduce__func__sum" ]
];