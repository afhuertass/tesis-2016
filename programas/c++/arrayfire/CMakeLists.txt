CMAKE_MINIMUM_REQUIRED(VERSION 3.0)
PROJECT(foo)
FIND_PACKAGE(ArrayFire REQUIRED)
FIND_PACKAGE(CUDA QUIET)
INCLUDE_DIRECTORIES(${ArrayFire_INCLUDE_DIRS})
 


IF (${CUDA_FOUND})
    IF(${ArrayFire_CUDA_FOUND})  # variable defined by FIND(ArrayFire ...)
        FIND_LIBRARY( CUDA_NVVM_LIBRARY
          NAMES "nvvm"
          PATH_SUFFIXES "nvvm/lib64" "nvvm/lib"
          PATHS ${CUDA_TOOLKIT_ROOT_DIR}
          DOC "CUDA NVVM Library"
          )
        MESSAGE(STATUS "EXAMPLES: CUDA backend is ON.")

	ADD_EXECUTABLE(pi main.cpp )
	TARGET_LINK_LIBRARIES(pi ${ArrayFire_CUDA_LIBRARIES} "${CUDA_CUBLAS_LIBRARIES};${CUDA_LIBRARIES};${CUDA_cusolver_LIBRARY};${CUDA_CUFFT_LIBRARIES};${CUDA_NVVM_LIBRARY};${CUDA_CUDA_LIBRARY}" )
	
	ENDIF()
	ENDIF()

