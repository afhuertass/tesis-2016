// aqui la clase encargada de crear las meshes 
/*
 mucho del codigo esta basado en el ejemplo 
Mesh_3/mesh_polyhedral_domain.cpp de la documentacion de CGAL
 */

// CGAL includes
// g++ mesh.cpp -lCGAL -lboost_system  -lboost_log -std=c++11 


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/refine_mesh_3.h>

// otras inclusiones
#include <fstream>
#include <iostream>

// poly

// definicion de tipos, dominio
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef CGAL::Polyhedral_mesh_domain_3<Polyhedron, K> Mesh_domain;


// Triangulation
#ifdef CGAL_CONCURRENT_MESH_3
typedef CGAL::Mesh_triangulation_3<
  Mesh_domain,
  CGAL::Kernel_traits<Mesh_domain>::Kernel, // Same as sequential
  CGAL::Parallel_tag                        // Tag to activate parallelism
  >::type Tr;
#else
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
#endif
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
typedef Mesh_criteria::Facet_criteria Facet_criteria;
typedef Mesh_criteria::Cell_criteria Cell_criteria;


// To avoid verbose function and named parameters call

using namespace CGAL::parameters;



class MeshHelper{
  // clase para generar Mesh3D a partir de polyhedro
public:
  MeshHelper(Polyhedron p);
  void saveMeshFile();
  void saveBoundary();
  bool isPointIn();
private:
  C3t3 mesh;
  
  void generateMesh(Polyhedron p);
  
  
};

MeshHelper::MeshHelper(Polyhedron p){
  // en el constructor generamos el mesh
  this->generateMesh(p);
  
    
}
void MeshHelper::generateMesh(Polyhedron p){
  Mesh_domain domain(p);
  Mesh_criteria criteria(facet_angle=25, facet_size=0.15, facet_distance=0.008,cell_radius_edge_ratio=3);
  // criterios, para ensayos, probamos con estos y luego extendemos.

  this->mesh = CGAL::make_mesh_3<C3t3>( domain , criteria , no_perturb(), no_exude()) ;
  

}
void MeshHelper::saveMeshFile(){
  
  std::ofstream medit_file("out.mesh");
  this->mesh.output_to_medit(medit_file);
  medit_file.close();

};
void MeshHelper::saveBoundary(){
  std::ofstream off_file("boundary.off");
  this->mesh.output_boundary_to_off(off_file);
  off_file.close();
  
}
bool MeshHelper::isPointIn(){
  // 
  
}
int main(){

  Modelo cubo("aa.obj");
  return 0;

}
