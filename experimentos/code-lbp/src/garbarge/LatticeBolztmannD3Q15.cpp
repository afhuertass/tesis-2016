#include <arrayfire.h>
#include <inc/headers/LatticeBolztmannD3Q15.h>


// Modelo 3d para lattice bolztman utlizando el metodo entrpico 

LatticeBolztmannD3Q15::LatticeBolztmannD3Q15(int lx,int ly, int lz){
  //constructor
  this->Lx=lx; // tamaño del lattice
  this->Ly=ly;
  this->Lz=lz;
  
  this->f = af::constant(0 , Lx , Ly, Lz, q , f32);
  // arreglo de funciones de equilibrio
  Vel_x = af::constant(0 , Lx , Ly, Lz, q , f32);
  Vel_y = af::constant(0 , Lx , Ly, Lz, q , f32);
  Vel_z = af::constant(0 , Lx , Ly, Lz, q , f32);
  

  w = af::constant(0 , q , f32);
  
  
}
LatticeBolztmannD3Q15::Inicie(float r0 , float Ux0 , float Uz0){
  
}
array LatticeBolztmannD3Q15::feq( array &rhos, array &Uxs , array &Uys , array &Uzs){

  
}
array LatticeBolztmannD3Q15::rhos(){
  
}


array LatticeBolztmannD3Q15::Jx(){
  
}
array LatticeBolztmannD3Q15::Jy(){
  
}
array LatticeBolztmannD3Q15::Jz(){
  
}

void LatticeBolztmannD3Q15::Colission(){
  
}

void LatticeBolztmannD3Q15::Adveccion(){
  
}

int main(){
  return 0;
}
