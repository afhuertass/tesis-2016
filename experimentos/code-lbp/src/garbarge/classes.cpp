#include "meshIncludes.h"


using namespace CGAL::parameters;
template <class HDS>
class Polyhedron_builder : public CGAL::Modifier_base<HDS>{
public:
  std::vector<double> &coords;
  std::vector<aiFace> &faces;
  Polyhedron_builder( std::vector<double> &c , std::vector<aiFace> &f) : coords(c) , faces(f){}
  void operator()(HDS& hds) {
    typedef typename HDS::Vertex Vertex;
    typedef typename Vertex::Point Point;
    CGAL::Polyhedron_incremental_builder_3<HDS> B(hds,false);
    
    B.begin_surface(coords.size()/3, faces.size()  );
    //  B.begin_surface( vertices , caras esperadas, h  );
    for(int i = 0; i < (int)coords.size() ; i+=3 ){
      B.add_vertex( Point( coords[i] , coords[i+1] , coords[i+2] ));
    }
    
    for ( int i = 0; i < (int) faces.size(); i+=1){
      B.begin_facet(); // Iteramos por cada cara agregando los indicesa el facet
      for ( int j = 0 ; j < faces[i].mNumIndices; j++) {
	B.add_vertex_to_facet( faces[i].mIndices[j] );
      } 
      B.end_facet(); // fin de la cara 
    }
    B.end_surface(); // construimos la superficie 
  }
  
  
};

 PolyhedronContainer::PolyhedronContainer(aiMesh* oMesh){
// en el constructor vamos a procesar la mesh ... llenar los vectores de coordenadas y generar el polyhedro de una buena puta vez. 

 for(int i = 0;  i < oMesh->mNumVertices; i++){
     coords.push_back(oMesh->mVertices[i].x );
     coords.push_back(oMesh->mVertices[i].y );
     coords.push_back(oMesh->mVertices[i].z );
    
 }
 // ahora guardar las caras en un arreglo
 
 for( int i = 0; i < oMesh->mNumFaces; i++){
   // por cada cara
   aiFace face = oMesh->mFaces[i];
   faces.push_back(face);
   
 }
 Polyhedron_builder<HalfedgeDS> builder(coords, faces);
 P.delegate(builder);
 }
Polyhedron PolyhedronContainer::getPoly(){
  return this->P;
}

Modelo::Modelo(std::string path){
  
  this->loadModelo(path);
  
}

void Modelo::loadModelo(std::string path){
  Assimp::Importer importer;
  const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
  if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) 
    {
      std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
      return;
    }
  // en esta parte el modelo esta cargado. y guardado en scene. debemos obteneer las mesh
  
  this->populatePolys(scene->mRootNode ,scene);
  
}
void Modelo::populatePolys(aiNode* node, const aiScene* scene){
  // esta cosa tiene que funcionar recursicamente.
  for ( int i = 0; i < node->mNumMeshes; i++){
    // iterar sobre todas las mesh del nodo actual
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i] ];
     PolyhedronContainer polC(mesh);
     polys.push_back(  polC.getPoly()  ); // un arreglo de los pol
  }
  
  for( int i = 0; i < node->mNumChildren; i++){
    
    this->populatePolys(node->mChildren[i] , scene );
    
  }
}
void Modelo::getOff(){
  // sacar archivos .off
  std::ofstream os;
  os.open("out.off");
  
  for(int i = 0; i < polys.size() ; i++){
    Polyhedron P = polys.at(0);
    
    os << P;
  }
  os.close();
}
Polyhedron_m Modelo::createPolyForMesh(){
  Polyhedron_m p;
  this->getOff();
   std::ifstream input("out.off");
   input >> p;
   return p;
}
MeshHelper::MeshHelper(Polyhedron_m p){
  // en el constructor generamos el mesh
  this->generateMesh(p);
  
}
void MeshHelper::generateMesh(Polyhedron_m p){
  Mesh_domain domain(p);
  Mesh_criteria criteria(facet_angle=25, facet_size=0.15, facet_distance=0.008,cell_radius_edge_ratio=3);
  // criterios, para ensayos, probamos con estos y luego extendemos.

  this->mesh = CGAL::make_mesh_3<C3t3>( domain , criteria , no_perturb(), no_exude()) ;
  

}
void MeshHelper::saveMeshFile(){
  
  std::ofstream medit_file("out.mesh");
  this->mesh.output_to_medit(medit_file);
  medit_file.close();

};
void MeshHelper::saveBoundary(){
  std::ofstream off_file("boundary.off");
  this->mesh.output_boundary_to_off(off_file);
  off_file.close();
  
}
bool MeshHelper::isPointIn(const Point  &query){
  // 
  Locate_type lt;
  int ii, jj;
  Tr &tr = mesh.triangulation();
  //Cell_handle infinite = tr.infite_cell();
  Cell_handle ch = tr.locate( query , lt , ii ,jj );
  if ( lt == Tr::OUTSIDE_AFFINE_HULL && !tr.is_infinite(ch)) {
    return false;
  }
  if(lt == Tr::CELL && !tr.is_infinite(ch)  ){
    return true;
  }else {
    return false;
  }
 
}
