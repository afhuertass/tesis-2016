
PROYECTO - MODELOS 3D Y SIMULACIONES DE LATTICE BOLZTMANN

El presente proyecto tiene por meta lograr la integracion de modelos geometricos 3d diseñados en programas especiales tales como (blender, autocad etc) en simulaciones de fisica utilizando el metodo de lattice-bolztmann. 

La motivacion de este proyecto es diseñar un metodo que permita abordar problemas de dinamica de  fluidos que se den en geometrias tan complicadas como se requiera, sin las limitantes usuales que se presentan a la hora de describir las condiciones de contorno o de obstaculos para objetos que no pueden ser descritos dentro del lattice con una ecuacion matematica. Y a esto agregarle la capacidad de realizar simulaciones por medio de librerias de alto rendimiento en GPUS. 


Comenzamos. 

---- 4 de noviembre ---- 

El proyecto posee la siguiente estrucutra:

Los archivos .cpp se guardan dentro de 

    code-lbp/src
	
los headers y las declaraciones de clases en 
    
    code-lbp/inc/headers

la compilacion la realiza un script de Cmake, el CMakeLists.txt se encuentra en code-lbp/

Compilar el programa requiere principalmente la libreria CGAL, y Assimp ( y obviamente todas las dependencias de estas dos ) pero bueno... el precio que hay que pagar es un largo tiempo de compilacion, este problema trata de ser resuelto por medio de una aplicacion de cmake llamada cotire, que puede ser encontrada en google en un bonito repositorio de github y que pretende acelerar el proceso de compilacion... 

 
    
	  
		
